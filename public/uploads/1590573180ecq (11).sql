-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 22, 2020 at 07:03 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ecq`
--

-- --------------------------------------------------------

--
-- Table structure for table `branch`
--

CREATE TABLE `branch` (
  `branch_id` int(11) NOT NULL,
  `branch_name` varchar(200) NOT NULL,
  `description` text NOT NULL,
  `branch_image` varchar(200) NOT NULL,
  `b_status` varchar(100) NOT NULL DEFAULT 'available',
  `is_hidden` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `branch`
--

INSERT INTO `branch` (`branch_id`, `branch_name`, `description`, `branch_image`, `b_status`, `is_hidden`, `created_at`, `updated_at`) VALUES
(1, 'Head Office', 'This is the head office', '', 'temporary closed', 0, '2020-05-04 14:46:41', '2020-05-04 14:46:41'),
(2, 'EDSA Shang Ri La', 'This is edsa shangrila branch', '', 'temporary closed', 0, '2020-05-04 14:46:41', '2020-05-04 14:46:41'),
(3, 'Landmark Alabang', 'This is viamare alabang', 'lm-alabang.jpg', 'available', 0, '2020-05-04 15:05:57', '2020-05-04 15:05:57'),
(4, 'Test Branch', 'DTest', '', 'available', 1, '2020-05-04 15:33:16', '2020-05-04 15:33:16'),
(5, 'Greenbelt 1', 'Greenbelt 1 Makati', 'gb1.jpg', 'available', 0, '2020-05-10 23:17:17', '2020-05-10 23:17:17'),
(6, 'Via Mare Landmark Makati', 'Via Mare Landmark Branch (Makati)', '', 'temporary closed', 0, '2020-05-10 23:17:49', '2020-05-10 23:17:49'),
(7, 'Via Mare Landmark Trinoma', 'Via Mare Landmark Branch (Trinoma)', '', 'temporary closed', 0, '2020-05-10 23:18:38', '2020-05-10 23:18:38'),
(8, 'NAIA Terminal 3', 'Via Mare T3', 'naiat3.jpg', 'available', 0, '2020-05-10 23:19:21', '2020-05-10 23:19:21'),
(9, 'Via Mare Rockwell', 'Rockwell Branch', 'rockwell.jpg', 'available', 0, '2020-05-17 22:50:53', '2020-05-17 22:50:53'),
(10, 'Test Branch', 'none', '', 'available', 1, '2020-05-21 14:56:25', '2020-05-21 14:56:25');

-- --------------------------------------------------------

--
-- Table structure for table `category`
--

CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `category_name` varchar(200) NOT NULL,
  `hidden` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `category`
--

INSERT INTO `category` (`category_id`, `category_name`, `hidden`, `created_at`, `updated_at`) VALUES
(1, 'Atlanghap', 0, '2020-04-30 19:36:03', '2020-04-30 19:36:03'),
(2, 'At Iba Pa', 0, '2020-04-30 19:36:03', '2020-04-30 19:36:03'),
(3, 'Grilled Pan De Sal', 0, '2020-05-01 00:20:55', '2020-05-01 00:20:55'),
(4, 'Ensalada At Gulay', 0, '2020-05-06 18:19:02', '2020-05-06 18:19:02'),
(5, 'Bibingka Lista', 1, '2020-05-21 15:02:44', '2020-05-21 15:02:44'),
(6, 'Mainit Na Sabaw', 0, '2020-05-21 15:21:19', '2020-05-21 15:21:19'),
(7, 'Masaganang Tanghalian', 0, '2020-05-21 15:21:33', '2020-05-21 15:21:33'),
(8, 'Mga Pampalamig', 0, '2020-05-21 15:21:40', '2020-05-21 15:21:40'),
(9, 'Sari-Saring Kakanin', 0, '2020-05-21 15:22:09', '2020-05-21 15:22:09'),
(10, 'Sari-Saring Kanin', 0, '2020-05-21 15:22:14', '2020-05-21 15:22:14'),
(11, 'beverages', 1, '2020-05-21 16:12:49', '2020-05-21 16:12:49');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL,
  `category_id` int(11) NOT NULL,
  `prod_name` varchar(200) NOT NULL,
  `prod_image` varchar(200) NOT NULL DEFAULT 'noimage.jpg',
  `prod_code` varchar(100) NOT NULL,
  `price` double NOT NULL,
  `menu_status` varchar(50) NOT NULL DEFAULT 'available',
  `stock` int(100) NOT NULL,
  `hidden` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`menu_id`, `category_id`, `prod_name`, `prod_image`, `prod_code`, `price`, `menu_status`, `stock`, `hidden`, `created_at`, `updated_at`) VALUES
(1, 1, 'Chicken and Pork Adobo', 'Chicken and Pork Adobo.jpg', 'ckenndprkadbo', 600, 'available', 0, 0, '2020-05-21 15:38:39', '2020-05-21 15:38:39'),
(2, 1, 'Corned Beef Saute', 'Corned Beef Saute.jpg', 'cornedbeefsaute', 250, 'available', 0, 0, '2020-05-21 15:54:22', '2020-05-21 15:54:22'),
(3, 1, 'Crisp Adobo Flakes', 'Crisp Adobo Flakes.jpg', 'crspadboflks', 240, 'available', 0, 0, '2020-05-21 15:59:12', '2020-05-21 15:59:12'),
(4, 2, 'Arroz Caldo Goto', 'Arroz Caldo Goto.jpg', 'arrozcaldogoto', 260, 'available', 0, 0, '2020-05-21 16:00:03', '2020-05-21 16:00:03'),
(5, 9, 'Bibingka Cassava', 'Bibingka Cassava.JPG', 'bbngkacassva', 195, 'available', 0, 0, '2020-05-21 16:01:20', '2020-05-21 16:01:20'),
(6, 10, 'Fiesta Rice', 'Fiesta Rice.jpg', 'fiestarice', 290, 'available', 0, 0, '2020-05-21 17:39:35', '2020-05-21 17:39:35'),
(7, 8, 'Shooters', 'Shooters.JPG', 'shooterspalamig', 110, 'available', 0, 0, '2020-05-21 17:40:16', '2020-05-21 17:40:16'),
(8, 7, 'Kare-Kare', 'Kare-Kare.jpg', 'karekare', 370, 'available', 0, 0, '2020-05-21 17:41:03', '2020-05-21 17:41:03'),
(9, 6, 'Sinigang na Tiyan ng Bangus', 'Sinigang na Tiyan ng Bangus.jpg', 'snignagnatyanbangus', 180, 'available', 0, 0, '2020-05-21 17:41:50', '2020-05-21 17:41:50'),
(10, 3, 'Laguna Cheese', 'Laguna Cheese.jpg', 'lagunachesse', 140, 'available', 0, 0, '2020-05-21 17:42:59', '2020-05-21 17:42:59'),
(11, 4, 'Rellenong Talong', 'Rellenong Talong.JPG', 'rellenongtalong', 320, 'available', 0, 0, '2020-05-21 17:43:33', '2020-05-21 17:43:33');

-- --------------------------------------------------------

--
-- Table structure for table `menu_settings`
--

CREATE TABLE `menu_settings` (
  `settings_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `menu_status` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `menu_settings`
--

INSERT INTO `menu_settings` (`settings_id`, `branch_id`, `menu_id`, `menu_status`) VALUES
(1, 1, 1, 'available'),
(2, 2, 1, 'out of stock'),
(3, 3, 1, 'out of stock'),
(4, 4, 1, 'out of stock'),
(5, 5, 1, 'available'),
(6, 6, 1, 'out of stock'),
(7, 7, 1, 'out of stock'),
(8, 8, 1, 'out of stock'),
(9, 9, 1, 'out of stock'),
(10, 10, 1, 'out of stock'),
(11, 1, 2, 'available'),
(12, 2, 2, 'available'),
(13, 3, 2, 'available'),
(14, 4, 2, 'available'),
(15, 5, 2, 'available'),
(16, 6, 2, 'available'),
(17, 7, 2, 'available'),
(18, 8, 2, 'available'),
(19, 9, 2, 'available'),
(20, 10, 2, 'available'),
(21, 1, 3, 'available'),
(22, 2, 3, 'available'),
(23, 3, 3, 'available'),
(24, 4, 3, 'available'),
(25, 5, 3, 'available'),
(26, 6, 3, 'available'),
(27, 7, 3, 'available'),
(28, 8, 3, 'available'),
(29, 9, 3, 'available'),
(30, 10, 3, 'available'),
(31, 1, 4, 'available'),
(32, 2, 4, 'available'),
(33, 3, 4, 'available'),
(34, 4, 4, 'available'),
(35, 5, 4, 'available'),
(36, 6, 4, 'available'),
(37, 7, 4, 'available'),
(38, 8, 4, 'available'),
(39, 9, 4, 'available'),
(40, 10, 4, 'available'),
(41, 1, 5, 'available'),
(42, 2, 5, 'available'),
(43, 3, 5, 'available'),
(44, 4, 5, 'available'),
(45, 5, 5, 'available'),
(46, 6, 5, 'available'),
(47, 7, 5, 'available'),
(48, 8, 5, 'available'),
(49, 9, 5, 'available'),
(50, 10, 5, 'available'),
(51, 1, 6, 'available'),
(52, 2, 6, 'available'),
(53, 3, 6, 'available'),
(54, 4, 6, 'available'),
(55, 5, 6, 'available'),
(56, 6, 6, 'available'),
(57, 7, 6, 'available'),
(58, 8, 6, 'available'),
(59, 9, 6, 'available'),
(60, 10, 6, 'available'),
(61, 1, 7, 'available'),
(62, 2, 7, 'available'),
(63, 3, 7, 'available'),
(64, 4, 7, 'available'),
(65, 5, 7, 'available'),
(66, 6, 7, 'available'),
(67, 7, 7, 'available'),
(68, 8, 7, 'available'),
(69, 9, 7, 'available'),
(70, 10, 7, 'available'),
(71, 1, 8, 'available'),
(72, 2, 8, 'available'),
(73, 3, 8, 'available'),
(74, 4, 8, 'available'),
(75, 5, 8, 'available'),
(76, 6, 8, 'available'),
(77, 7, 8, 'available'),
(78, 8, 8, 'available'),
(79, 9, 8, 'available'),
(80, 10, 8, 'available'),
(81, 1, 9, 'available'),
(82, 2, 9, 'available'),
(83, 3, 9, 'available'),
(84, 4, 9, 'available'),
(85, 5, 9, 'available'),
(86, 6, 9, 'available'),
(87, 7, 9, 'available'),
(88, 8, 9, 'available'),
(89, 9, 9, 'available'),
(90, 10, 9, 'available'),
(91, 1, 10, 'available'),
(92, 2, 10, 'available'),
(93, 3, 10, 'available'),
(94, 4, 10, 'available'),
(95, 5, 10, 'available'),
(96, 6, 10, 'available'),
(97, 7, 10, 'available'),
(98, 8, 10, 'available'),
(99, 9, 10, 'available'),
(100, 10, 10, 'available'),
(101, 1, 11, 'available'),
(102, 2, 11, 'available'),
(103, 3, 11, 'available'),
(104, 4, 11, 'available'),
(105, 5, 11, 'available'),
(106, 6, 11, 'available'),
(107, 7, 11, 'available'),
(108, 8, 11, 'available'),
(109, 9, 11, 'available'),
(110, 10, 11, 'available');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `order_id` int(11) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `mobile_no` bigint(100) NOT NULL,
  `address` varchar(200) NOT NULL,
  `firstname` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `notes` text NOT NULL,
  `del_method` varchar(100) NOT NULL,
  `pup_time` varchar(200) NOT NULL DEFAULT 'n/a',
  `total` double NOT NULL,
  `status` int(100) NOT NULL DEFAULT 0,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_details`
--

CREATE TABLE `order_details` (
  `od_id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `qty` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_status`
--

CREATE TABLE `order_status` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL,
  `role` varchar(100) NOT NULL,
  `fname` varchar(100) NOT NULL,
  `lname` varchar(100) NOT NULL,
  `mobile_no` bigint(100) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `branch_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `role`, `fname`, `lname`, `mobile_no`, `email`, `password`, `branch_id`, `created_at`, `updated_at`) VALUES
(2, 'administrator', 'micko jel', 'dela rosa', 123, 'mickojel.delarosa@viamare.com.ph', '123', 3, '2020-05-03 20:14:44', '2020-05-03 20:14:44'),
(3, 'cashier', 'Albert', 'Dukati', 9092969518, 'a.ducati@gmail.com', '123', 2, '2020-05-11 01:16:12', '2020-05-11 01:16:12'),
(4, 'administrator', 'Ricardo', 'Dalisay', 9095969518, 'vmalabang@gmail.com', '123', 3, '2020-05-11 03:07:45', '2020-05-11 03:07:45'),
(5, 'administrator', 'James', 'Balikbayan', 909565928, 'vmgreenbelt@gmail.com', '123', 5, '2020-05-11 03:07:45', '2020-05-11 03:07:45'),
(6, 'administrator', 'Elsa', 'Kalesa', 909595629, 'vmlmakati@gmail.com', '123', 6, '2020-05-11 03:08:56', '2020-05-11 03:08:56'),
(7, 'administrator', 'Taylor', 'Swift', 9095968174, 'vmltrinoma@gmail.com', '123', 7, '2020-05-11 03:08:56', '2020-05-11 03:08:56'),
(8, 'administrator', 'Eishia', 'Konno', 9564234762, 'vmnaia@gmail.com', '123', 8, '2020-05-11 03:09:26', '2020-05-11 03:09:26'),
(999, 'administrator', 'micko jel', 'dela rosa', 9092969518, 'admin@viamare.com.ph', '123', 1, '2020-05-03 20:14:29', '2020-05-03 20:14:29');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `branch`
--
ALTER TABLE `branch`
  ADD PRIMARY KEY (`branch_id`);

--
-- Indexes for table `category`
--
ALTER TABLE `category`
  ADD PRIMARY KEY (`category_id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`),
  ADD KEY `category_id` (`category_id`);

--
-- Indexes for table `menu_settings`
--
ALTER TABLE `menu_settings`
  ADD PRIMARY KEY (`settings_id`),
  ADD KEY `branch_id` (`branch_id`),
  ADD KEY `menu_id` (`menu_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`order_id`),
  ADD KEY `branch_id` (`branch_id`);

--
-- Indexes for table `order_details`
--
ALTER TABLE `order_details`
  ADD PRIMARY KEY (`od_id`),
  ADD KEY `menu_id` (`menu_id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `order_status`
--
ALTER TABLE `order_status`
  ADD PRIMARY KEY (`id`),
  ADD KEY `order_id` (`order_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`),
  ADD KEY `branch_id` (`branch_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `branch`
--
ALTER TABLE `branch`
  MODIFY `branch_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `category`
--
ALTER TABLE `category`
  MODIFY `category_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `menu_settings`
--
ALTER TABLE `menu_settings`
  MODIFY `settings_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=111;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `order_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_details`
--
ALTER TABLE `order_details`
  MODIFY `od_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_status`
--
ALTER TABLE `order_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1000;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`category_id`) REFERENCES `category` (`category_id`);

--
-- Constraints for table `menu_settings`
--
ALTER TABLE `menu_settings`
  ADD CONSTRAINT `menu_settings_ibfk_1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`branch_id`),
  ADD CONSTRAINT `menu_settings_ibfk_2` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`menu_id`);

--
-- Constraints for table `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `orders_ibfk_1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`branch_id`);

--
-- Constraints for table `order_details`
--
ALTER TABLE `order_details`
  ADD CONSTRAINT `order_details_ibfk_1` FOREIGN KEY (`menu_id`) REFERENCES `menu` (`menu_id`),
  ADD CONSTRAINT `order_details_ibfk_2` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`);

--
-- Constraints for table `order_status`
--
ALTER TABLE `order_status`
  ADD CONSTRAINT `order_status_ibfk_1` FOREIGN KEY (`order_id`) REFERENCES `orders` (`order_id`);

--
-- Constraints for table `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_ibfk_1` FOREIGN KEY (`branch_id`) REFERENCES `branch` (`branch_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
