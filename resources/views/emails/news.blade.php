@component('mail::message')
# Via Mare Updates From HR



@component('mail::panel')

{{$data['title']}}

{!! html_entity_decode($data['content']) !!}

@endcomponent

@component('mail::button', ['url' => 'www.viamare.com.ph/delivery'])
View On Website
@endcomponent


Please do not reply to this email!


Thanks,<br>
{{ config('app.name') }}

<br>

CONFIDENTIALITY NOTICE: The information transmitted through this mail is intended solely for the person or entity to which it is addressed, and may contain confidential and/or privileged material. Any review, retransmission, disclosure, dissemination or other use of, or taking of any action in reliance upon, this information by persons or entities other than the intended recipient is prohibited. Please notify the sender immediately by e-mail if you have received this e-mail by mistake and delete this e-mail from your system.

WARNING: Computer viruses can be transmitted via email. The recipient should check this email and any attachments for the presence of viruses. The sender therefore does not accept liability for any errors or omissions in the contents of this message, which arise because of e-mail transmission.
@endcomponent
