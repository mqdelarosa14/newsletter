@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <a href="/email" class="btn btn-primary btn-block mb-4">Go back</a>
            
            
            <form action="/email" method="post">
                @include('layouts.form_email')
              <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection
