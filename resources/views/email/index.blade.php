@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <a href="/email/create" class="btn btn-primary btn-block mb-4">Register An Email</a>
            

            <table id="emails_table" class="table table-striped table-bordered" style="width: 100%">
                <thead class="thead-light">
                    <tr>
                        <th>ID</th>
                        <th>Email</th>
                        <th>Tag</th>
                        <th>View Details</th>
                        <th>Update</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($emails as $email)
                    <tr>
                        <td>{{ $email->id }}</td>
                        <td>{{ $email->email_address }}</td>
                        <td>{{ $email->email_tag }}</td>
                        <td><a href="/email/{{$email->id}}" class="btn btn-success btn-sm">Show Details</a></td>
                        <td><a href="/email/{{$email->id}}/edit" class="btn btn-primary btn-sm">Update</a></td>
                    </tr>
                    @empty

                    <div class="card shadow rounded">
                        <div class="card-header">No Email Registered</div>
                        <div class="card-body">
        
                        </div>
                    </div>
                        
                    @endforelse
                </tbody>
            </table>
                

        </div>
    </div>
</div>
@endsection
