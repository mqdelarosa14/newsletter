@extends('layouts.app')

@section('content')
<div class="container overflow" >
    <div class="row justify-content-center">
        <div class="col-md-8">
            <a href="/newsletter/create" class="btn btn-primary btn-block mb-4">Create News</a>
            
            @forelse ($news as $new)

            <div class="card shadow rounded mt-4">
                <div class="card-header">{{ $new->title }}</div>
                <div class="card-body">
                    <div class="media">
                        <div class="media-body">
                            <p>{!! $new->content !!}</p>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <a href="/newsletter/{{ $new->id }}" class="btn btn-success btn-sm">Show More</a>
                    <a href="/newsletter/{{ $new->id }}/edit" class="btn btn-primary btn-sm">Update</a>
                    Date Created <small>{{ $new->created_at }}</small>
                </div>
            </div>
                
            @empty

            <div class="card shadow rounded">
                <div class="card-header">No News Available</div>
                <div class="card-body">

                </div>
            </div>
                
            @endforelse

            <div class="row mt-5">
                <div class="col-12 d-flex justify-content-center">
                    {{ $news->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
