@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <a href="/newsletter" class="btn btn-primary btn-block mb-4">Go back</a>
            
            
            <form action="/newsletter/{{ $news->id}}" method="post">
                @method('PATCH')
                @include('layouts.form')
              <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>
@endsection
