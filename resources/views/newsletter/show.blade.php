@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card shadow rounded mt-4">
                <div class="card-header">{{ $news->title }}</div>
                <div class="card-body">
                    <div class="media">
                        <div class="media-body">
                            <p>{!! $news->content !!}</p>
                        </div>
                    </div>
                </div>
                <div class="card-footer">
                    <form action="/newsletter/{{$news->id}}" method="post">
                    <a href="/newsletter" class="btn btn-danger btn-sm">Back</a>
                    <a href="/newsletter/{{$news->id}}/edit" class="btn btn-primary btn-sm">Update</a>
                    
                        @method('DELETE')
                        @csrf

                        <button class="btn btn-danger btn-sm">Delete</button>
                    
                    </form>
                    Date Created <small>{{ $news->created_at }}</small>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
