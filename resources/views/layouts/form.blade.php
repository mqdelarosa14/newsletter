@csrf
<div class="form-group">
  <label for="title">News Title</label>
  <input type="text" class="form-control" id="title" name="title" aria-describedby="titleHelp" value="{{ old('title') ?? $news->title }}">
  @error('title')
      <div class="alert alert-danger alert-dismissible fade show" role="alert">
          <small>{{ $message }}</small>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
      </div>
  @enderror
</div>

<div class="form-group">
  <label for="file">Attach a file</label>
  <input type="file" multiple="multiple" class="form-control" id="file" name="file[]" aria-describedby="fileHelp" value="{{ old('file') ?? $news->file }}">
  @error('file')
      <div class="alert alert-danger alert-dismissible fade show" role="alert">
          <small>{{ $message }}</small>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
      </div>
  @enderror
</div>

<div class="form-group">
  <label for="content">News Content</label>
  <textarea class="form-control content" id="content" name="content" rows="3">{{ old('content') ?? $news->content }}</textarea>
  @error('content')
      <div class="alert alert-danger alert-dismissible fade show" role="alert">
          <small>{{ $message }}</small>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
      </div>
  @enderror
</div>
