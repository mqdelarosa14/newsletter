@csrf

<div class="form-group">
  <label for="name">Full Name</label>
  <input type="text" class="form-control" id="name" name="name" placeholder="Last Name, Full Name M.I." aria-describedby="nameHelp" value="{{ old('name') ?? $emails->name }}">
  @error('name')
      <div class="alert alert-danger alert-dismissible fade show" role="alert">
          <small>{{ $message }}</small>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
      </div>
  @enderror
</div>


<div class="form-group">
  <label for="email_address">Email</label>
  <input type="text" class="form-control" id="email_address" name="email_address" aria-describedby="email_addressHelp" value="{{ old('email_address') ?? $emails->email_address }}">
  @error('email_address')
      <div class="alert alert-danger alert-dismissible fade show" role="alert">
          <small>{{ $message }}</small>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
      </div>
  @enderror
</div>

<div class="form-group">
  <label for="email_tag">Tag (Manager, Branch, Etc.)</label>
  <input type="text" class="form-control" id="email_tag" name="email_tag" aria-describedby="email_tagHelp" value="{{ old('email_tag') ?? $emails->email_tag }}">
  @error('email_tag')
      <div class="alert alert-danger alert-dismissible fade show" role="alert">
          <small>{{ $message }}</small>
          <button type="button" class="close" data-dismiss="alert" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
      </div>
  @enderror
</div>



