<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Email;

class EmailsController extends Controller
{
    function index(){

        $emails = Email::orderBy('created_at', 'DESC')->get();

        // dd($news);

        return view('email.index', compact('emails'));
    }

    function __construct(){
        $this->middleware('auth');
    }
    
    function create(){
        $emails = new Email();
        return view('email.create', compact('emails'));
    }

    function store(){

        $emails = Email::create($this->validatedData());

        return redirect('/email/' . $emails->id);
    }

    function show(Email $emails){

        return view('email.show', compact('emails'));
    }

    function edit(Email $emails){
        return view('email.edit', compact('emails'));
    }

    function update(Email $emails){
        
        $emails->update($this->validatedData());

        return redirect('/email');
    }

    function destroy(Email $emails){
        $emails->delete();
        return redirect('/email');
    }

    protected function validatedData(){
        return request()->validate([
            'name' => 'required',
            'email_address' => 'required|email|unique:emails',
            'email_tag' => 'required|min:4'
        ]);
    }
}
