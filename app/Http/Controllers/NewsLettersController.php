<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\NewsMail;
use App\NewsLetter;
use App\Email;

use Intervention\Image\Facades\Image;

class NewsLettersController extends Controller
{
    function index(){

        $news = NewsLetter::orderBy('created_at', 'DESC')->paginate(5);

        return view('newsletter.index', compact('news'));
    }

    function __construct(){
        $this->middleware('auth');
    }
    
    function create(){
        $news = new NewsLetter();
        return view('newsletter.create', compact('news'));
    }

    function store(){

        $news = NewsLetter::create($this->validatedData());
        
        foreach(request()->file as $file){

            $news->update([
                'file' => $file->store('images','public'),
            ]);
            $file_name = $file->getClientOriginalName();    
            $file_path = 'uploads/';
            // dd($file_name);
            $file->move($file_path, $file_name);

        }

        ini_set('max_execution_time', 0);

        $emails = Email::all();

        foreach($emails as $email){

            Mail::to($email->email_address)->send(new NewsMail($this->validatedData()));

        }
        
        

        return redirect('/newsletter/' . $news->id);
    }

    function show(NewsLetter $news){

        return view('newsletter.show', compact('news'));
    }

    function edit(NewsLetter $news){
        return view('newsletter.edit', compact('news'));
    }

    function update(NewsLetter $news){
        
        $news->update($this->validatedData());

        return redirect('/newsletter');
    }

    function destroy(NewsLetter $news){
        $news->delete();
        return redirect('/newsletter');
    }

    protected function validatedData(){
        return request()->validate([
            'title' => 'required',
            'content' => 'required',
        ]);
    }

    private function storeImage($news){

            $files = $this->validatedData('file');
        
            foreach ($files as $key => $file) { 
                $news->update([
                    'file' => request()->file->store('images','public'),
                ]);
            }

    }
}
