<?php

namespace App\Mail;



use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


use App\NewsLetter;

class NewsMail extends Mailable
{
    use Queueable, SerializesModels;
    public $data;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        // return $this->markdown('emails.news')->attach(public_path().'/robots.txt');

        // dd($this->data['file']); // array of all files to be attached 

        // $files = $this->data['file'];

        // dd(request()->file);

    
        $message = $this->markdown('emails.news');    
        
        foreach (request()->file as $file) { 
            // dd($file,$key);
            // $message->attach($file); // attach each file
            $filename = $file->getClientOriginalName();
            $message->attach(public_path().'/uploads/'. $filename); // attach each file
            // $message->attach(public_path().'/uploads/'. '2.png'); // attach each file
            // $message->attach(public_path().'/uploads/'. '3.jpg'); // attach each file
        }

        return $message; //Send mail
    }
}
