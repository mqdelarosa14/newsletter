<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/home', function () {
//     return view('newsletter.index');
// });

Route::get('/', 'NewsLettersController@index');

Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');
//for News Letter Controller
Route::get('/newsletter', 'NewsLettersController@index');
Route::get('/newsletter/create', 'NewsLettersController@create');
Route::post('/newsletter', 'NewsLettersController@store');
Route::get('/newsletter/{news}', 'NewsLettersController@show');
Route::get('/newsletter/{news}/edit', 'NewsLettersController@edit');
Route::patch('/newsletter/{news}', 'NewsLettersController@update');
Route::delete('/newsletter/{news}', 'NewsLettersController@destroy');
// end news letter controller routes

//for Email Controller
Route::get('/email', 'EmailsController@index');
Route::get('/email/create', 'EmailsController@create');
Route::post('/email', 'EmailsController@store');
Route::get('/email/{emails}', 'EmailsController@show');
Route::get('/email/{emails}/edit', 'EmailsController@edit');
Route::patch('/email/{emails}', 'EmailsController@update');
Route::delete('/email/{emails}', 'EmailsController@destroy');
// end Email controller routes
